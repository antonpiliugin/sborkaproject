{% macro headScripts() %}
	<script type="text/javascript">
		;(function(){
			var modificators = [],
				nv = navigator,
				pl = nv.platform.toLowerCase(),
				ua = nv.userAgent.toLowerCase(),
				an = nv.appName.toLowerCase(),
				htmlElement = document.documentElement,
				ls = localStorage;

			function pli(s){return pl.indexOf(s) > -1;}
			function uai(s){return ua.indexOf(s) > -1;}
			function ani(s){return an.indexOf(s) > -1;}

			// Platform
			var platformClass = uai('iphone') || uai('ipod') || uai('ipad') || uai('android') || uai('iemobile') || uai('blackberry') || uai('bada') ? '_mobile' : '_desktop';
			modificators.push(platformClass);

			// OS
			var osClass = '';
			if (uai('ipad') || uai('iphone') || uai('ipod')) {
				osClass = '_ios';
			} else if (uai('android')) {
				osClass = '_android';
			} else if (pli('win')) {
				osClass = '_win';
			} else if (pli('mac')) {
				osClass = '_mac';
			} else if (pli('linux')) {
				osClass = '_linux';
			}
			modificators.push(osClass);

			// Browser
			var browserClass = '';
			if (uai('firefox')) {
				browserClass = '_ff';
			} else if (uai('opr')) {
				browserClass = '_opera';
			} else if (uai('yabrowser')) {
				browserClass = '_yandex';
			} else if (uai('edge')) {
				browserClass = '_edge';
			} else if (uai('trident') || ani('explorer') || ani('msie')) {
				browserClass = '_ie';
			} else if (uai('safari') && !uai('chrome')) {
				browserClass = '_safari';
			} else if (uai('chrome')) {
				browserClass = '_chrome';
			}
			modificators.push(browserClass);

			window.environmentObject = {
				platform: platformClass,
				os: osClass,
				browser: browserClass,
				isLocal: window.location.href.indexOf('http://localhost:') == 0
			};
			htmlElement.className += ' ' + modificators.join(' ');

			// SVG sprites
			var xhr = new XMLHttpRequest();
			xhr.open('GET', 'assets/svg/sprite.svg', true);
			xhr.overrideMimeType('image/svg+xml');

			xhr.onreadystatechange = function(){
				if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
					function waitForBody(){
						if(document.body){
							var containerClass = 'invisible-container';
							var container = document.querySelector('.'+containerClass);
							if(!container){
								container = document.createElement('div');
								container.className = containerClass;
								document.body.insertBefore(container, document.body.firstChild);
							}
							container.appendChild(xhr.responseXML.documentElement);
						} else {
							setTimeout(waitForBody, 16);
						}
					}

					waitForBody();
				};
			}

			xhr.send('');
		})();

		var cartData = {{ cart | dump | safe }};
		var cartCounter;
		var basketList;
		var subtotalElement;
		var taxElement;
		var shippingElement;
		var totalElement;

		document.addEventListener('DOMContentLoaded', function() {
			cartCounter = document.getElementById('menu-cart-counter');
			cartCounter.innerText = cartData.length;

			basketList = document.getElementById('basket-list');
			var basketTotal = document.getElementById('basket-total');
			subtotalElement = basketTotal.querySelector('div.cart-price.basket-total__subtotal span.cart-price__price_value');
			taxElement = basketTotal.querySelector('div.cart-price.basket-total__tax span.cart-price__price_value');
			shippingElement = basketTotal.querySelector('div.cart-price.basket-total__shipping span.cart-price__price_value');
			totalElement = basketTotal.querySelector('div.cart-price.basket-total__total span.cart-price__price_value');
		});

		function updateCart(id, qty) {
			var index = cartData.findIndex(function(product) { return product.id === id });
			if (index > -1) {
				var item = cartData[index];
				var newQty = item.qty + qty;
				var element = basketList.children[index];
				if (newQty < 1 || qty === 0) {
					basketList.children[index].remove();
					cartData.splice(index, 1);
					cartCounter.innerText = cartData.length;
				} else {
					var qtyElement = element.querySelector('span.product-item__left__middle_controls_qty_text');
					if (qtyElement) qtyElement.innerText = newQty;
					item.qty = newQty;
				}

				var subtotal = cartData.reduce(function(acc, product) { return acc + (product.qty * product.price) }, 0);
				var tax = parseInt(taxElement.innerText.replace(' ', ''), 10);
				var shipping = parseInt(shippingElement.innerText.replace(' ', ''), 10);
				var total = subtotal + tax + shipping;

				subtotalElement.innerText = moneyFormat(subtotal);
				if (subtotal === 0) {
					taxElement.innerText = 0;
					shippingElement.innerText = 0;
					totalElement.innerText = 0;
				} else {
					taxElement.innerText = 100;
					shippingElement.innerText = 150;
					totalElement.innerText = moneyFormat(total);
				}
			}
		}

		function moneyFormat(x) {
			var parts = x.toString().split('.');
			parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
			return parts.join('.');
		}

		function menuClick(id) {
			console.log('CLICK', id);
		}
	</script>
{% endmacro %}
